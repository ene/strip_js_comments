use std::path::PathBuf;

fn main() {
    // The env var gets set by the nix flake
    let dir: PathBuf = PathBuf::from(format!("{}/src", env!("TREE_SITTER")));

    cc::Build::new()
        .include(&dir)
        .file(dir.join("parser.c"))
        .file(dir.join("scanner.c"))
        .compile("tree-sitter-javascript");
}
