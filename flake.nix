{
  description = "A js comment remover, powered by tree-sitter";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    crane.url = "github:ipetkov/crane";
    crane.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    tree_sitter_js = {
      url = "github:tree-sitter/tree-sitter-javascript";
      flake = false;
    };
  };

  outputs = {
    self,
    nixpkgs,
    crane,
    flake-utils,
    tree_sitter_js,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      craneLib = crane.lib.${system};
      craneBuild = craneLib.buildPackage {
        src = craneLib.cleanCargoSource ./.;

        doCheck = true;
        buildInputs = [
          tree_sitter_js.outPath
        ];
        preBuild = ''
          export TREE_SITTER=${tree_sitter_js};
        '';
      };
    in {
      packages.default = craneBuild;
      legacyPackages.default = craneBuild;
      app.default = {
        type = "app";
        program = "${self.packages.${system}.default}/bin/strip_js_comments";
      };
    });
}
# vim: ts=2

