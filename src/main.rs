use std::{env, fs, process};

use tree_sitter::{Language, Parser, TreeCursor};
extern "C" {
    fn tree_sitter_javascript() -> Language;
}

struct Range {
    start: usize,
    end: usize,
}

fn main() {
    // get the cli args, for the file path
    let path_to_js_file = match env::args().nth(1) {
        Some(out) => out,
        None => {
            eprintln!("Missing the path to the file!");
            process::exit(1);
        }
    };

    // initialize the parser
    let mut parser = Parser::new();
    let language = unsafe { tree_sitter_javascript() };
    if let Err(error) = parser.set_language(language) {
        eprintln!(
            "Hit an error while setting the language for the pareser: {}",
            error
        );
        process::exit(1)
    };

    // read file and parse it
    let source_code = match fs::read_to_string(&path_to_js_file) {
        Ok(out) => out,
        Err(error) => {
            eprintln!("Hit an error while reading {}: {}", path_to_js_file, error);
            process::exit(1)
        }
    };
    let parsed_source_code = match parser.parse(&source_code, None) {
        Some(out) => out,
        None => {
            eprintln!("Your parsed File seems empty, or at least there is nothing to parse!");
            process::exit(1);
        }
    };

    // filter out the content
    let mut cursor = parsed_source_code.walk();
    let mut expressions: Vec<Range> = vec![];
    if let Some(range) = find_expression(&cursor) {
        expressions.push(range);
    };
    while cursor.goto_first_child() {
        if let Some(range) = find_expression(&cursor) {
            expressions.push(range);
        };
        while cursor.goto_next_sibling() {
            if let Some(range) = find_expression(&cursor) {
                expressions.push(range);
            };
        }
    }

    // turn the filtered output to a printable string
    let bytes_of_source_code: Vec<u8> = source_code.bytes().collect();
    let mut final_string = String::new();
    for range in expressions {
        final_string.push_str(&format!(
            "{}\n",
            match std::str::from_utf8(&bytes_of_source_code[range.start..range.end]) {
                Ok(str) => {
                    str
                }
                Err(e) => {
                    eprintln! {"There is some non utf-8 bytes in your string: {e}"};
                    process::exit(1)
                }
            }
        ));
    }
    println!("{}", final_string);
}

fn find_expression(cursor: &TreeCursor) -> Option<Range> {
    let node = cursor.node();
    if node.kind() == "expression_statement" {
        Some(Range {
            start: node.start_byte(),
            end: node.end_byte(),
        })
    } else {
        None
    }
}
